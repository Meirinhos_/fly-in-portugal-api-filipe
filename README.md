# Fly-In-Portugal API

## General Description
* **FlyInPortugalAPI:** server initialization and /flight/avg endpoint declaration;
* **FlightService:** service that handles calls made to the /flight endpoint. Right now with the /avg being the only available route;
* **SkypickerFlightsExternalService:** wrapper around the Skypicker search API.

##### Maven packages used:
* **Spark (spark-core):** web framework;
* **Gson (gson):** Java serialization/deserialization library to convert Java Objects into JSON and back 
* **Apache HttpClient (httpclient):** used tool to construct URI's

## General Architecture, Decisions and Assumptions

Endpoints available:

* **/flight/avg** - calculate *average flight price* and *average bag price* information in a set of flights

To use the endpoint 4 parameters must be provided:
* from: 3-digit code of the city from where the flight departs;
* to: 3-digit code of the city from where the flight lands;
* date_from: start date of the flights;
* date_to: end date of the flights;

REQUEST (example)
```
/flight/avg?from=OPO&to=LIS&date_from=12%2F03%2F2020&date_to=14%2F03%2F2020
```

RESPONSE (example)
```
{
    "city_from": "Porto",
    "city_code_from": "OPO",
    "city_to": "Lisbon",
    "city_code_to": "LIS",
    "n_flights": 40,
    "currency": "EUR",
    "avg_price": 83,
    "avg_bag_prices": {
        "1": 47,
        "2": 121
    }
}
```

All the prices being requested are in euros and it is indicated so in the response

Only Ryanair and TAP flights are being considered by providing the parameter 'select_airlines' to the Skypicker Search API with the respective airline codes ('FR' and 'TP')

Even though it was requested to only consider the Lisbon <-> Porto route, the solution allows to request information about other routes by passing the **to** and **from** parameters.


#### SkypickerFlightsExternalService:

Created to wrap requests to the Skypicker search API.

Right now the only available method is **searchFlights(...)** which makes a request to Skypicker's /flight endpoint.
A collection of **FlightInfo** objects which contain relevant information is returned to further be processed. This structure is also handy because it allows for the response to be deserialized.

This service implements the **FlightsExternalService** interface. The interface allows the creation of Mock Services to be used for testing purposes. 

#### FlightService

Responsible for providing the methods to handle the flight resource.

Right now it handles the gathering of aggregate information to be provided as a response to the /flight/avg endpoint.

Along with the parameters needed to execute a search, this service is also given an external flight search service, one that implements the **FlightsExternalService** interface. This way testing can be done using Mock Services.

With the information collected from the external source, this service aggregates the information accordingly and retrieves a structure that can be then serialized and redirected to the client.

## Improvements
* Better error handling (e.g. retry logic and error passing between services);
* Add support for other currencies - easy to do, it replicates some logic already used;


## Changelog
* 12-Fev-2020 Filipe Fernandes