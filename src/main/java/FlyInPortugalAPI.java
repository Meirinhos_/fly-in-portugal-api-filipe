import com.google.gson.*;

import flight.FlightService;
import flight.external.FlightsExternalService;
import flight.FlightRouteAggregate;
import flight.external.SkypickerFlightsExternalService;

import javax.management.AttributeNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import static spark.Spark.*;

public class FlyInPortugalAPI {

    public static void main(String[] args) {

        Gson gson = new Gson();

        path("flight", () -> {

            get("/avg", "application/json", (request, response) -> {
                response.type("application/json");

                // Create external service object to injent in hadler service
                FlightsExternalService flightsExternalService = new SkypickerFlightsExternalService();

                // Instantiate service responsible for handling the request
                FlightService flightService = null;
                try {
                    flightService = new FlightService(flightsExternalService, request);
                } catch (AttributeNotFoundException e) { // missing attributes
                    e.printStackTrace();
                    halt(400, e.getMessage());
                } catch (IllegalArgumentException e) { // ill-formatted attributes
                    e.printStackTrace();
                    halt(422, e.getMessage());
                }

                // Use the service to handle the request
                FlightRouteAggregate routeAgregattes = null;
                try {
                    routeAgregattes = flightService.getRouteAverages();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    halt(422, e.getMessage());
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                    halt(503, "Service is temporarly unavailable");
                } catch (URISyntaxException e) {
                    halt(500, "Please contact the administrator");
                    e.printStackTrace();
                }

                return routeAgregattes;
            }, gson::toJson); // Convert returned object to JSON

        });


    }

}
