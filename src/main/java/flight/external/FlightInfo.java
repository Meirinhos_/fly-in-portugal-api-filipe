package flight.external;

import java.util.Map;

public class FlightInfo {

    public int price;
    public String cityFrom, cityTo;
    public Map<String, Integer> bags_price;

    public FlightInfo() {}

}
