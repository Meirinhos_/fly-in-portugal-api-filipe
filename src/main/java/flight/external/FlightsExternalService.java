package flight.external;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;

public interface FlightsExternalService {

    default FlightInfo[] searchFlights(String cityCodeFrom, String cityCodeTo, LocalDate dateFrom, LocalDate dateTo) throws URISyntaxException, IOException, IllegalArgumentException, InterruptedException {
        return null;
    }

    String getDatePattern();

}
