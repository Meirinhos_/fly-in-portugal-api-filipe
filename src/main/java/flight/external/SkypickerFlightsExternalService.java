package flight.external;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.http.client.utils.URIBuilder;

import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static javax.ws.rs.core.Response.Status.Family.CLIENT_ERROR;
import static javax.ws.rs.core.Response.Status.Family.SERVER_ERROR;


public class SkypickerFlightsExternalService implements FlightsExternalService {

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .connectTimeout(Duration.ofSeconds(10))
            .build();

    private final static String externalServicePartner = "picky";
    private final static String serviceBaseURL = "https://api.skypicker.com/";
    private final static String datePattern = "d/MM/yyyy";
    private final static String supportedAirlines = "FR,TP";

    public SkypickerFlightsExternalService() {}

    @Override
    public FlightInfo[] searchFlights(
            String cityCodeFrom,
            String cityCodeTo,
            LocalDate dateFrom,
            LocalDate dateTo
    ) throws URISyntaxException, IOException, IllegalArgumentException, InterruptedException {

        URI uri = new URIBuilder(serviceBaseURL)
                .setPath("/flights")
                .addParameter("fly_from", cityCodeFrom)
                .addParameter("fly_to", cityCodeTo)
                .addParameter("date_from", dateFrom.format(DateTimeFormatter.ofPattern(datePattern)))
                .addParameter("date_to", dateTo.format(DateTimeFormatter.ofPattern(datePattern)))
                .addParameter("select_airlines", supportedAirlines)
                .addParameter("partner", externalServicePartner)
                .build();

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(uri)
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() == 200) {
            return new GsonBuilder().create().fromJson(getJsonArray(response.body(), "data"), FlightInfo[].class);
        } else if (Response.Status.Family.familyOf(response.statusCode()) == CLIENT_ERROR) {
            if (response.statusCode() == 400 || response.statusCode() == 422) {
                JsonObject error = getJsonArray(response.body(), "message").get(0).getAsJsonObject();
                String param = error.get("param").getAsString();
                String errorMsg = error.get("errors").getAsJsonArray().get(0).getAsString();
                throw new IllegalArgumentException(param + " - " + errorMsg);
            } else {
                throw new IOException(""); // error on our side, force 503
            }
        } else if (Response.Status.Family.familyOf(response.statusCode()) == SERVER_ERROR) {
            throw new IOException(); // Server error, force 503 on our side
        } else {
            throw new IOException(); // Something exceptional went wrong, force 503 on our side
        }

    }

    @Override
    public String getDatePattern() {
        return datePattern;
    }

    private JsonArray getJsonArray(String jsonData, String key) {
        JsonObject rootObj = JsonParser.parseString(jsonData).getAsJsonObject();
        return rootObj.getAsJsonArray(key);
    }

}
