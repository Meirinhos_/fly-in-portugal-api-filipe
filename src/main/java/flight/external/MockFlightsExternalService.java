package flight.external;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;

// Mock Service to be used for testing puposes
public class MockFlightsExternalService implements FlightsExternalService {

    private final static String datePattern = "d/MM/yyyy";

    public MockFlightsExternalService() {}

    @Override
    public FlightInfo[] searchFlights(String cityCodeFrom, String cityCodeTo, LocalDate dateFrom, LocalDate dateTo) throws URISyntaxException, IOException, InterruptedException {
        return new FlightInfo[0];
    }

    @Override
    public String getDatePattern() {
        return datePattern;
    }
}
