package flight;

import flight.external.FlightInfo;
import flight.external.FlightsExternalService;

import javax.management.AttributeNotFoundException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

import static java.util.stream.Collectors.*;

public class FlightService {

    private final Set<String> paramSet = new HashSet<>(Arrays.asList("from", "to", "date_from", "date_to"));
    private final String currency = "EUR";

    private String cityCodeFrom, cityCodeTo;
    private LocalDate dateFrom, dateTo;

    private FlightsExternalService flightsExternalService;

    public FlightService(FlightsExternalService service, spark.Request request) throws AttributeNotFoundException, IllegalArgumentException {

        // Validations
        validateParamsPresence(request.queryParams());
        for (String s : paramSet)
            validateParamValuePresence(s, request.queryParams(s));

        // Assignments
        this.cityCodeFrom = request.queryParams("from");
        this.cityCodeTo = request.queryParams("to");
        this.dateFrom = parseDateParam("date_from", request.queryParams("date_from"), service.getDatePattern());
        this.dateTo = parseDateParam("date_to", request.queryParams("date_to"), service.getDatePattern());

        // Date range validation
        if (this.dateTo.compareTo(this.dateFrom) < 0)
            throw new IllegalArgumentException("'date_to' (" + this.dateTo.toString() + ") should not be inferior than 'date_from' (" + this.dateFrom.toString() + ")");

        this.flightsExternalService = service;

    }

    // Looks up flights within the parameters provided, computes aggregations and returns a structure with the relevant information
    public FlightRouteAggregate getRouteAverages() throws URISyntaxException, IOException, InterruptedException, IllegalArgumentException {

        // Contact external API to gather flight data
        FlightInfo[] flightsInfo = flightsExternalService.searchFlights(cityCodeFrom, cityCodeTo, dateFrom, dateTo);

        if (flightsInfo.length == 0) // External service returned no results
            return new FlightRouteAggregate("", this.cityCodeFrom, "", this.cityCodeTo, this.currency, 0, 0, null);

        // Calculate average flight price
        int avgPrice = Arrays.stream(flightsInfo)
                .reduce(0, (sum, ob) -> sum + ob.price, Integer::sum)
                / flightsInfo.length;

        // Calculate average bag price
        Map<String, Integer> bagsPrice = Arrays.stream(flightsInfo)
                .map(p -> p.bags_price)
                .flatMap(m -> m.entrySet().stream())
                .collect(groupingBy(Map.Entry::getKey, summingInt(Map.Entry::getValue)));
        bagsPrice.replaceAll((key, val) -> val / flightsInfo.length);

        String cityFrom = Arrays.stream(flightsInfo).map(p -> p.cityFrom).distinct().findFirst().get();
        String cityTo = Arrays.stream(flightsInfo).map(p -> p.cityTo).distinct().findFirst().get();

        return new FlightRouteAggregate(cityFrom, this.cityCodeFrom, cityTo, this.cityCodeTo, this.currency, avgPrice, flightsInfo.length, bagsPrice);
    }

    private void validateParamsPresence(Set<String> requestParams) throws AttributeNotFoundException {
        if (!requestParams.containsAll(paramSet))
            throw new AttributeNotFoundException(String.format("(%s) parameters must be present", String.join(", ", paramSet)));
    }

    private void validateParamValuePresence(String param, String paramValue) throws AttributeNotFoundException {
        if (paramValue == null || paramValue.length() == 0)
            throw new AttributeNotFoundException(String.format("'%s' parameter value must be present", param));
    }

    private LocalDate parseDateParam(String param, String date, String pattern) throws IllegalArgumentException{
        try {
            return LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern));
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException(String.format("'%s: %s' is not a valid date", param, date));
        }
    }


}
