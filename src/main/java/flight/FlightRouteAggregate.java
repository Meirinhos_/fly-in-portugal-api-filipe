package flight;

import java.util.Map;

public class FlightRouteAggregate {

    public String city_from, city_code_from, city_to, city_code_to;
    public int n_flights;
    public String currency;
    public int avg_price;
    public Map<String, Integer> avg_bag_prices;

    public FlightRouteAggregate(String city_from, String city_code_from, String city_to, String city_code_to, String currency, int avg_price, int n_flights, Map<String, Integer> avg_bag_prices) {
        this.avg_price = avg_price;
        this.n_flights = n_flights;
        this.currency = currency;
        this.city_from = city_from;
        this.city_code_from = city_code_from;
        this.city_to = city_to;
        this.city_code_to = city_code_to;
        this.avg_bag_prices = avg_bag_prices;
    }
}
